# # # Checklist Rider THE GAME # # # 

You are a game-tester and have to do checklist by the end of the day.
You want to do it riding on your chair, so beware of your bosses!
Just a fun game concept, made up with sentimentality & great friends :)



### How do I get set up? ###

* Using latest LTS Unity by the day - 2019.4.17f1
* Using URP
* Using WebGL -> The game will be build to work on internet site
* Using only free assets -> thanks for them :)
* Using tests -> Core mechanics will be divided into assemblies for UnityTesting! Any update? -> tests re-run recommended!

* Scenes:
	- GameScene -> start with it
	- LevelScene -> Just a LEVEL/MAP. GameScene have a logic to load it (additive).

* Branches:
	- release/master -> Only working game here
	- game_core -> basicly it's the main dev branch
	- game_levelDesign -> all the level will be done here (not to interrupt other branches/scenes)



					---------------- BASE DEPENDENCY MAP ------------------




				OnScene									LazySingletons
		(should be on Scene to Init/ref properly)				 (only created when needed for the 1st time)
--------------------------------------------------------------------------------	--------------------------------------------------------------------


					GlobalManager.cs
			 (Ref to All BaseData & Creating all LazySingletons)
						|				
				________________|_______________________________			
				|						|--------------	GameDataManager.cs  (manager for GameData (SO))
			GameController.cs					|
		 (Base On-Scene singleton with all Refs)			|--------------	PlayerData.cs  (Player prefs here -> mute/highscore etc)
				|						|
				|						|--------------	AudioManager.cs  (Cover almost all audio with Pool & Poolables)
	________________________|_______________________________
	|			|				|
TaskController.cs	CharController.cs		TimeController.cs (UI)







### Contribution guidelines ###

* Code review would be much appreciated!



### Special Thanks ###

* Nata Gora -> For great intro graphics :)
* Mortus for unique mainCharacter animations !
* Kenney for Audio (Sounds & Music Jingles) & UI_PACK assets

* for Asset: 'AS_Casual_Island_Game_Loops'
* for Asset: 'LowPolyOfficeProps_LITE'
* for Asset: 'simple modular human'
* for Asset: '_SNAPS_PrototypingAssets'
* for Asset: 'Polygonal Particles'




### UpdateLog & info: ###

* 2020/12/30 -> created repository with needed branches and descriptions
* 2020/12/30 -> added initial Game Files & Initial Scenes

* 2021/01/23 -> Level scene is completed in terms of prefabs/variants/model & optimization.
		Required prefabs are localized on the scene RESEMBLING the real office (the real office - not a coincidence)
		Optimized in terms of materials & hierarchy giving us just a few batches & SetPass calls for hundreds of items in a camera view.
		STATIC MESH OPTIMIZATION fully optimized & working (giving great optimization) - All required meshes are left enabled(for absent & present co-workers).
		
* 2021/01/23 -> Added inner-gallery
		Created Custom Forward Renderer using stencil for mask & see through objects (URP)
		Created & used simple custom shader + Layer for mask
		SeeThroughMask Layer is for 'inner objects'

* 2021/01/24 -> Added base code structure (some abstract patterns like Singletons)
		Global Managers (singletons) will handle some base logic like:
			- AudioManager - handle audio & mixers. Quite simple, it's not a 3d game with 3d sounds, so this kind of manager will do the job
			- GameData - handle all data in game
			- PlayerData - handle Player statistics/settings which can be set by the player
		OnSceneManger (scene singleton):
			- GameController -> handle 'game' initialization, have base refs, handle game states ETC.

* 2021/01/24-25	-> Added LIGHTS to the LevelScene
		! ! ! BROUGHT BACK SRP BATCHER -> now it helps with reflection probes batching
		Optimized some assets for Probes/Lightmap scales etc
		Whole level is static, so we baked lightmaps, added some ambient lights/emissive
		We also will use 'mixed' global light for SHADOWS of moving characters (only main & enemy BOSSes)
		Added LightProbes
		Added ReflectionProbes

* 2021/02/01	-> Added templates for ALL characters
		-> Modular human free asset is used
		-> Special animations were added for main character -> retargeted
		-> Whole think is a mess! NO optimization... Many single textures/materials -> Probably won't be fixed :) external assets/anims ):


* 2021/02/04-05	-> * * * DESIGN PATTERNS & GAME MANAGEMENT 
		! ! ! 'GlobalManager' will be used as a GLOBAL singleton which will hold all required references like data SO etc. Game won't use resources but WILL NEED the GameManager on main scenes (GameScene to be exact)
		Added/adjusted 4 design patterns that will be used in the project
			1. Singleton & PeristentLazySingleton - for global managers (AudioManager/PlayerData/GameData etc)
			2. StateMachine - for NPC states handling etc (like scouting NPCs/AI states)
			3. Factory - for more 'generic' object instantiating like preset-audioSources prefabs...
			4. Pooling - co-working with Factory Pattern with eg. AudioSources generic-instantiating & proper pooling


* 2021/02/11	-> Added MainCharacter base controllers implementation & models & animations
		We can ride at the scene, push, bounce from walls


* 2021/02/12	-> Adjusted LevelScene for lightmapping
		Baked new lightmaps with new settings

* 2021/02/12	-> MainChar Refactor & OnScene spawning
		Made a little refactor/clean-up/using some delegates as UnityActions -> ease for future changes
		'workplace template' was created with 'chair' reference point -> for GameController to find the place after the levelScene is loaded & put the player in it's place

* 2021/02/14	-> NPCs Spawning & little refactor
		Added GlobalEnums for work/character types
		Added distinguishing between CTOs
		Adjusted Workplaces
		Added NPC_Controller to Find proper workplaces & Spawn them on their places
		GameData works little simplier now
		
* 2021/02/18	-> AI NPCs -> CTOs LOGIC ADDED
		2 different logic for 2 ctos -> G Type & D Type
		Using State Machine for recovery state & chasing state
		Using SO as external data file for some AI params
		Everything with ICheckpointable interface can be referenced as the target to chase (for now player & some points on Scene)

* 2021/02/19	-> GameController as StateMachine
		Added states: Starting, Playing, GameWin, GameOver with proper methods & refs
		Added some basic DataConnection like slowDown & screen transitions params

* 2021/02/19	->Added FOV Effector scripts
		Added proper Layer Masking for effector to renderer PipeLine
		Effector can check if Target is in range


* 2021/02/19	-> Added Mentual.Misc Assembly (for various utility scripts)
		Added TESTS for FoV_Effector which is in assembly now (Editor & Play Mode)

* 2021/02/20	-> Added HUD to UI 
		static & dynamic Canvases
		Fill Bars (with assembly & Tests) for death & task progress
		Clock on HUD to show currentTime (8-16)


* 2021/02/21	Added task UX
		Characters with tasks can wave now & continue working while we're nearby
		FOV Effector now changes color for Enemies (orange - looking for us / red - we'rre in range) & workspaces (yellow - waiting / green - working)

* 2021/02/21	Added UiSprites
		Packed them into 1 SpriteAtlas (1 atlas for all UI sprites!) - batching/optimization
		Added 2d sprite package (editing sprite /9-slices etc)
		'ReDesigned' fillBars UI/ ending screen / task screens -> UI is quite new now...


* 2021/02/21	Added Clock Controller
		Clock UI
		We can see highscores now (clock hour)


* 2021/02/21	UI CLEAN-UP (prefabs etc)


* 2021/02/21	Added VFX
		Push Particles as Pool & poolable
		Flames/smoke for working/waiting -> better indicator for 'task' character
		Little flames from chair's wheel -> bigger & faster with speed increase


* 2021/02/22	Added Audio
		Properly imported (duration to usage ratio)
		Added AudioManager
		Tweaked Mixers (effects/ducking etc)
		Added AudioPools/types/Poolables for easy reusing

* 2021/02/23	Adjustements After feedback:
		- UI adjustements & optimization (raycast targets off etc)
		- Easier Pushing & more bounciness
		- Added current task description
		- Sound tweaks (works soooo weird in WebGL... completele different than norally)
		- Mixer Tweaks (music is separated to melody & drum group). Only 1 effect, NO DUCKING (no need here... only uses processing power)


* 2021/02/23	WebGL Build settings


* 2021/02/24	Clean-ups & Refactors & Optimizations
		- Optimized FOV Effector GC allocation
		- disabled serializing on many fields (which were for dev/debug info)
		- some clean-up/old asset deletion etc


* 2021.02/25	* * * RELEASE * * *
		- Last Adjustements
		- Last Refactor
		- Last clean-ups
		- Merging Branches
		- Publishing ;)


GL & HF !
