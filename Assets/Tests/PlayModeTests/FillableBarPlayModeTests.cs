﻿using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class FillableBarPlayModeTests
    {

        // // // Fields
        FillableBar bar;

        int testEventCounter;
        void TestEvent()
        {
            testEventCounter++;
        }

        // // // Initing/Ending
        [SetUp]
        public void SetUp()
        {
            bar = new GameObject("FillableBar").AddComponent<FillableBar>();
            bar.FillableImage = bar.gameObject.AddComponent<Image>();
            bar.OnDepleted += TestEvent;
            bar.OnFilled += TestEvent;

            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 10f,
            };

            bar.BarData = barData;
        }

        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(bar.gameObject);
        }


        // // // Tests
        [UnityTest]
        public IEnumerator AutoDepleted()
        {
            testEventCounter = 0;

            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 7f,
                autoFill = true,
                autoFillVal = -15f,
            };

            bar.BarData = barData;
            bar.ResetBar();

            yield return new WaitForSeconds(1.1f);

            bool isDepleted = bar.IsDepleted;
            Assert.IsTrue(isDepleted);
        }

        [UnityTest]
        public IEnumerator AutoFilled()
        {
            testEventCounter = 0;

            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 15f,
            };

            bar.BarData = barData;
            bar.ResetBar();

            yield return new WaitForSeconds(1.1f);

            bool ifFilled = bar.IsFilled;
            Assert.IsTrue(ifFilled);
        }


        [UnityTest]
        public IEnumerator AutoFillWithEvents()
        {
            testEventCounter = 0;

            //Fill 1
            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 115f,
            };

            bar.BarData = barData;
            bar.ResetBar();

            yield return new WaitForSeconds(0.2f);

            bool IsFilled1 = bar.IsFilled;


            //Deplete 1
            FillableBarData barData2 = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = -115f,
            };
            bar.BarData = barData2;

            yield return new WaitForSeconds(0.2f);

            bool isDepleted = bar.IsDepleted;


            //Fill 2
            FillableBarData barData3 = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 115f,
            };

            bar.BarData = barData3;

            yield return new WaitForSeconds(0.2f);

            bool IsFilled2 = bar.IsFilled;


            // UnityAction counter check
            bool eventCounter = testEventCounter == 3;


            //Check
            Assert.IsTrue(IsFilled1 && isDepleted && IsFilled2 && eventCounter);
        }

    }

}