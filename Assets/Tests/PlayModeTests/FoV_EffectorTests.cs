﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class FoV_EffectorTests
    {
        // // // Fields
        FoVEffector foVEffector;
        Transform target;


        // // // Initing/Ending
        [SetUp]
        public void SetUp()
        {
            var fovGO = new GameObject("FoVEffector");
            foVEffector = fovGO.AddComponent<FoVEffector>();

            FOV_EffectorValueObject fovData = new FOV_EffectorValueObject
            {
                obstacleLayerMask = LayerMask.GetMask("Walls"),
                origin = Vector3.zero,
                startingAngle = 10f,
                fov = 360f,
                rayCount = 360,
                viewDistance = 10f,
            };

            target = new GameObject("go").transform;

            foVEffector.Init(fovData, target);
        }

        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(foVEffector.gameObject);
            Object.DestroyImmediate(target.gameObject);
        }


        // // // Tests
        [UnityTest]
        public IEnumerator TargetInRangeButObstacle()
        {
            //Create Obstacle Between
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = new Vector3(2f, 0f, 0f);
            cube.layer = LayerMask.NameToLayer("Walls");

            target.position = new Vector3(6f, 0f, 0f);

            yield return new WaitForSeconds(1f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsFalse(isInRange);
        }

    }

}