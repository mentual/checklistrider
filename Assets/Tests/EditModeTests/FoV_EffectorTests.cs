﻿using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    public class FoV_EffectorTests
    {
        // // // Fields
        FoVEffector foVEffector;
        Transform target;


        // // // Initing/Ending
        [SetUp]
        public void SetUp()
        {
            var fovGO = new GameObject("FoVEffector");
            foVEffector = fovGO.AddComponent<FoVEffector>();

            FOV_EffectorValueObject fovData = new FOV_EffectorValueObject
            {
                obstacleLayerMask = LayerMask.GetMask("Walls"),
                origin = Vector3.zero,
                startingAngle = 10f,
                fov = 360f,
                rayCount = 360,
                viewDistance = 10f,
            };

            target = new GameObject("go").transform;

            foVEffector.Init(fovData, target);
        }

        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(foVEffector.gameObject);
            Object.DestroyImmediate(target.gameObject);
        }


        // // // Tests
        [Test]
        public void TargetInRange()
        {
            target.position = new Vector3(6f, 0f, 0f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsTrue(isInRange);
        }

        [Test]
        public void TargetOutOfRange()
        {
            target.position = new Vector3(1000f, 1000f, 1000f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsFalse(isInRange);
        }

        [Test]
        public void TargetInRangeButObstacle()
        {
            //Move away fovEffector, cuz physics collider need to wait for frame to refresh
            foVEffector.transform.position = new Vector3(-1f, 0f, 0f);

            //Create Obstacle Between
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = new Vector3(2f, 0f, 0f);
            cube.layer = LayerMask.NameToLayer("Walls");

            target.position = new Vector3(6f, 0f, 0f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsFalse(isInRange);
        }

        [Test]
        public void TargetInRangeObstacleBehind()
        {
            //Create Obstacle Between
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = new Vector3(8f, 0f, 0f);
            cube.layer = LayerMask.NameToLayer("Walls");

            target.position = new Vector3(9f, 0f, 0f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsTrue(isInRange);
        }

        [Test]
        public void TargetInRangeObstacleInTheSamePosition()
        {
            //Create Obstacle Between
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = new Vector3(6f, 0f, 0f);
            cube.layer = LayerMask.NameToLayer("Walls");

            target.position = new Vector3(6f, 0f, 0f);

            bool isInRange = foVEffector.CheckTargetVisibility();
            Assert.IsTrue(isInRange);
        }

    }

}