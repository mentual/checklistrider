﻿using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace Tests
{
    public class FillableBarEditorTests
    {

        // // // Fields
        FillableBar bar;


        // // // Initing/Ending
        [SetUp]
        public void SetUp()
        {
            bar = new GameObject("FillableBar").AddComponent<FillableBar>();
            bar.FillableImage = bar.gameObject.AddComponent<Image>();

            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 5f,
            };

            bar.BarData = barData;
        }

        [TearDown]
        public void TearDown()
        {
            Object.DestroyImmediate(bar.gameObject);
        }


        // // // Tests
        [Test]
        public void InitiallyDepleted()
        {
            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 0f,
                autoFill = true,
                autoFillVal = 5f,
            };

            bar.BarData = barData;
            bar.ResetBar(true);

            bool isDepleted = bar.IsDepleted;
            Assert.IsTrue(isDepleted);
        }

        [Test]
        public void InitiallyFilled()
        {
            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 10f,
                autoFill = true,
                autoFillVal = 5f,
            };

            bar.BarData = barData;
            bar.ResetBar(true);

            bool IsFilled = bar.IsFilled;
            Assert.IsTrue(IsFilled);
        }

        [Test]
        public void InitiallyNotFilledNorDepleted()
        {
            FillableBarData barData = new FillableBarData
            {
                maxValue = 10f,
                startingFill = 5f,
                autoFill = true,
                autoFillVal = 5f,
            };

            bar.BarData = barData;
            bar.ResetBar();

            bool IsFilled = bar.IsFilled;
            bool isDepleted = bar.IsDepleted;
            Assert.IsTrue(!IsFilled && !isDepleted);
        }

    }

}