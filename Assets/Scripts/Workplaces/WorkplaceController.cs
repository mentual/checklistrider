﻿using UnityEngine;
using static GameEnums;

public class WorkplaceController : MonoBehaviour
{

    // // // Fields
    [SerializeField] GameObject propsAbsent;
    [SerializeField] GameObject propsPresent;
    [SerializeField] FoVEffector fovEffector;
    public SpawnableCharactersType characterToSpawn;
    public CharacterTaskTypes workCharacterTaskType;
    public Transform chairTr;

    public bool occupied;
    public bool alreadyDoneTask;

    BaseCharacterController currentChar;


    // // // Unity
    private void Update()
    {
        if (!fovEffector.gameObject.activeSelf)
            return;

        if (fovEffector.IsTargetInRange)
        {
            currentChar.SetTaskState(TaskStatusType.CURRENT);
        }
        else
        {
            currentChar.SetTaskState(TaskStatusType.WAITING);
        }
    }


    // // // Methods
    public void Activate(BaseCharacterController characterInstance)
    {
        // Position character on chair
        characterInstance.transform.position = chairTr.position;
        characterInstance.transform.rotation = chairTr.rotation;

        occupied = true;
        alreadyDoneTask = false;

        propsAbsent.SetActive(false);
        propsPresent.SetActive(true);

        currentChar = characterInstance;
    }


    /// <summary>
    /// In Case they're not already deactivated ON SCENE
    /// </summary>
    public void Deactivate()
    {
        propsAbsent.SetActive(true);
        propsPresent.SetActive(false);
        occupied = false;
        alreadyDoneTask = false;

        fovEffector.OnTargetInRange = null;
        fovEffector.gameObject.SetActive(false);
        currentChar = null;
    }


    public void InitTask()
    {
        fovEffector.gameObject.SetActive(true);

        fovEffector.OnTargetInRange += TaskProgress;

        fovEffector.Init(GameDataManager.Instance.gameData.charactersData.fovEffectorData, GameController.Instance.CharController.MainChar.transform);

        currentChar.SetTaskState(TaskStatusType.WAITING);
    }

    public void CompleteTask()
    {
        alreadyDoneTask = true;
        fovEffector.OnTargetInRange -= TaskProgress;

        fovEffector.gameObject.SetActive(false);

        currentChar.SetTaskState(TaskStatusType.DONE);
    }

    public void TaskProgress()
    {
        GameController.Instance.TaskProgress(Time.deltaTime);
    }

}
