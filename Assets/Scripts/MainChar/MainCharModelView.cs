﻿using UnityEngine;

public class MainCharModelView : MonoBehaviour
{

    // // // Hashes
    int pushTrigger = Animator.StringToHash("tPush");
    int moveBool = Animator.StringToHash("bMove");
    int rotateBool = Animator.StringToHash("bRotating");

    // // // Fields
    [Header("Refs")]
    [SerializeField] Transform characterModel;
    [SerializeField] Transform chairModel;
    public Transform pushCheckTr;
    [SerializeField] Animator animator;
    [SerializeField] ParticleSystem dustParticles;


    [Header("RootPosChange")]
    [Tooltip("Use to FORCE character root bone position")]
    public bool forcePosition;
    public Vector3 forcedPosiiont;
    public Transform forcedBone;


    // // // Unity
    private void LateUpdate()
    {
        if (forcePosition)
        {
            forcedBone.localPosition = forcedPosiiont;
        }
    }


    // // // Methods
    public void Rotate(Vector3 forward, bool active)
    {
        transform.forward = forward;
        animator.SetBool(rotateBool, active);
    }

    public void SetMovingActive(bool active)
    {
        animator.SetBool(moveBool, active);
    }

    public void Push()
    {
        animator.SetTrigger(pushTrigger);
    }

    public void ChangeDustSizeVelo(float velocityMag)
    {
        var mainSystem = dustParticles.main;
        mainSystem.startSpeed = Mathf.Clamp(velocityMag * velocityMult / speedMinMax.y, speedMinMax.x, speedMinMax.y);
        mainSystem.startSize = Mathf.Clamp(velocityMag * sizeMult / sizeMinMax.y, sizeMinMax.x, sizeMinMax.y);

    }

    public Vector2 speedMinMax;
    public float velocityMult;
    public float sizeMult;
    public Vector2 sizeMinMax;

}
