﻿using System.Collections;
using UnityEngine;
using static GameEnums;

[RequireComponent(typeof(Rigidbody))]
public class MainCharPhysicsController : MonoBehaviour
{

    // // // Fields
    MainCharController charController;
    MainCharDataSO charData;
    Transform pushCheckTr;

    // // // prv Fields
    Rigidbody rb;

    Coroutine pushCoroutine;
    WaitForSeconds pushDelayWait; // Micro optimization


    // // // Unity
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (charController.IsMoving)
        {
            Move();
        }
    }


    // // // Methods
    public void Init(MainCharController mainController, Transform pushCheckTr, MainCharDataSO charData)
    {
        this.charController = mainController;
        this.pushCheckTr = pushCheckTr;
        this.charData = charData;

        //Micro optimization -> craeting GC only once ;)
        pushDelayWait = new WaitForSeconds(charData.pushDelay);

        //CheckDefaultSettings();
    }

    public void Move()
    {
        rb.AddForce(-charController.CurrentForward * charData.moveStrengthMag * Time.fixedDeltaTime, ForceMode.Force);
    }

    public void Push()
    {
        if (pushCoroutine != null)
            StopCoroutine(pushCoroutine);

        pushCoroutine = StartCoroutine(PushC());
    }

    IEnumerator PushC()
    {
        yield return pushDelayWait;

        if (Physics.Raycast(pushCheckTr.position, pushCheckTr.forward, out RaycastHit hit, charData.pushDisntanceCheck, charData.pushLayers))
        {
            rb.AddForce(-charController.CurrentForward * charData.pushForceMag, ForceMode.Impulse);

            ObstaclesTypes obstacleType = ObstaclesTypes.DEFAULT;
            if (hit.transform.CompareTag("WallGlass"))
            {
                obstacleType = ObstaclesTypes.GLASS;
            }
            else if (hit.transform.CompareTag("WallC"))
            {
                obstacleType = ObstaclesTypes.CONCRETE;
            }

            charController.CreatePushParticles(hit.point, obstacleType);
        }
    }


    /// <summary>
    /// Just for debug / in-case
    /// </summary>
    void CheckDefaultSettings()
    {
        //Configure rb for best movement experience & optimization
        rb.interpolation = RigidbodyInterpolation.Interpolate; // For smoother character movement
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous; // just in case for very dynamic/speedy collisions - shouldt be the case in that game

        //We won't use gravity and constraint mainobject rotation/posY -> good optimization for that game
        rb.useGravity = false;
    }

    public Vector3 GetVelocity()
    {
        return rb.velocity;
    }

}
