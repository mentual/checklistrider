﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using static GameEnums;

/// <summary>
/// The class should handle all main character behaviours
/// It's a 'base controller' with all required references
/// </summary>
public class MainCharController : BaseCharacterController, ICheckpointable
{

    // // // Fields
    [Header("Refs")]
    [SerializeField] MainCharInputController inputController;
    [SerializeField] MainCharPhysicsController physicsController;
    [SerializeField] MainCharModelView modelView;
    [SerializeField] MainCharSoundController soundController;

    [SerializeField] PushParticlePool particlePool;

    // private
    MainCharDataSO charData;

    //Rotation
    Vector3 lastWantedForward;

    private Vector3 currentForward;
    public Vector3 CurrentForward { get => currentForward; }

    // Moving
    bool isMoving;
    public bool IsMoving { get => isMoving; }

    //Pushing
    bool canPush = true;
    Coroutine pushCoroutine;
    WaitForSeconds pushWait; // Micro optimization

    //Delegates
    // bool -> if moving is activated/deactivated
    public UnityAction<bool> OnSetMovingActive;
    public UnityAction OnPush;



    // // // Unity
    private void Start()
    {
        Init();
    }

    private void Update()
    {
        Rotate(); //Rotating should be handling in 'normal Update'
        modelView.ChangeDustSizeVelo(physicsController.GetVelocity().magnitude);
        soundController.SetFootstepSoundActive(isMoving && canPush);
    }


    // // // Interface Layer
    public Vector3 GetDestination()
    {
        return transform.position;
    }


    // // // Methods
    public void Init()
    {
        //Get character data
        charData = GameDataManager.Instance.gameData.mainCharData;

        //Initalizing sub-scripts
        inputController.Init(this);
        physicsController.Init(this, modelView.pushCheckTr, charData);

        //Micro optimization -> craeting GC only once ;)
        pushWait = new WaitForSeconds(charData.pushCD);

        //Self params init
        lastWantedForward = modelView.transform.forward;
        currentForward = lastWantedForward;

        SubscribeEvents();

        StartCoroutine(DelayMainCharacterInput(charData.inputDelay));
    }

    void SubscribeEvents()
    {
        OnPush += physicsController.Push;
        OnPush += modelView.Push;

        OnSetMovingActive += modelView.SetMovingActive;
    }

    IEnumerator DelayMainCharacterInput(float delay)
    {
        inputController.enabled = false;
        yield return new WaitForSeconds(delay);
        inputController.enabled = true;
    }

    #region Rotating
    /// <summary>
    /// Save wantedForward, that it's never 0
    /// Funky movement style, when char always fully rotate by 90 deg :)
    /// </summary>
    void Rotate()
    {
        Vector3 newForward = new Vector3(lastWantedForward.x, 0, lastWantedForward.z);

        newForward = Vector3.RotateTowards(currentForward, newForward, Mathf.Deg2Rad * charData.maxRotationPerSecond * Time.deltaTime, charData.maxRotationPerSecond);

        var angleDiff = Vector3.Angle(currentForward, newForward);
        bool canRotate = angleDiff > 1f;

        //Model Handle
        modelView.Rotate(newForward, canRotate);

        currentForward = newForward;
    }

    public void RotateInput(Vector3 wantedForward)
    {
        if (wantedForward.sqrMagnitude <= 0f)
            return;

        lastWantedForward = wantedForward;
    }
    #endregion

    #region Moving
    public void StartMoving()
    {
        isMoving = true;

        OnSetMovingActive?.Invoke(IsMoving);
    }

    public void StopMoving()
    {
        isMoving = false;

        OnSetMovingActive?.Invoke(IsMoving);
    }
    #endregion

    #region Pushing
    public void Push()
    {
        if (canPush)
        {
            if (pushCoroutine != null)
                StopCoroutine(pushCoroutine);
            pushCoroutine = StartCoroutine(PushDelayC());

            soundController.SetFootstepSoundActiveImmediate(false);

            OnPush?.Invoke();
        }
    }

    IEnumerator PushDelayC()
    {
        canPush = false;
        yield return pushWait;
        canPush = true;
    }

    public void CreatePushParticles(Vector3 position, ObstaclesTypes obstacleType)
    {
        var poolableParticle = particlePool.GetInstance();
        poolableParticle.Init(position, obstacleType);

        soundController.Push(position, obstacleType);
    }
    #endregion


    // Misc
    public void SetInputActive(bool active)
    {
        if (!active)
        {
            soundController.SetFootstepSoundActiveImmediate(false);
            isMoving = false;
        }

        inputController.enabled = active;
    }

}
