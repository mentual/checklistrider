﻿using System.Collections;
using UnityEngine;
using static GameEnums;

public class MainCharSoundController : MonoBehaviour
{

    // // // Fields
    [SerializeField] AudioSource footstepSource;
    [SerializeField] AudioClip footstepClip;
    [SerializeField] Vector2 footstepVolMinMax;
    [SerializeField] float footstepVolMixTime;

    bool footstepSound;
    Coroutine footstepActivationCoro;



    // // // Methods
    public void SetFootstepSoundActiveImmediate(bool active)
    {
        float finishVol = active ? footstepVolMinMax.y : footstepVolMinMax.x;
        footstepSource.volume = finishVol;
    }

    public void SetFootstepSoundActive(bool active)
    {
        if (footstepSound != active)
        {
            footstepSound = active;

            if (footstepActivationCoro != null)
                StopCoroutine(footstepActivationCoro);

            footstepActivationCoro = StartCoroutine(SetFootstepActive(active));
        }
    }

    IEnumerator SetFootstepActive(bool active)
    {
        float timer = 0f;
        float startingVol = footstepSource.volume;
        float finishVol = active ? footstepVolMinMax.y : footstepVolMinMax.x;

        while (timer < footstepVolMixTime)
        {
            float t = timer / footstepVolMixTime;
            float newVol = Mathf.Lerp(startingVol, finishVol, t);

            footstepSource.volume = newVol;

            timer += Time.deltaTime;

            yield return null;
        }


        footstepSource.volume = finishVol;
    }


    public void Push(Vector3 position, ObstaclesTypes obstacleType)
    {
        switch (obstacleType)
        {
            case ObstaclesTypes.DEFAULT:
                AudioManager.Instance.PlayDefaultHit(position);
                break;
            case ObstaclesTypes.GLASS:
                AudioManager.Instance.PlayGlassHit(position);
                break;
            case ObstaclesTypes.CONCRETE:
                AudioManager.Instance.PlayConcreteHit(position);
                break;
        }
    }

}
