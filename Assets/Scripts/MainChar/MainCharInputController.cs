﻿using UnityEngine;

public class MainCharInputController : MonoBehaviour
{

    // // // Fields
    MainCharController charController;


    // // // Unity
    private void Update()
    {
        HandleInput();
    }


    // // // Methods
    public void Init(MainCharController mainController)
    {
        this.charController = mainController;
    }

    void HandleInput()
    {
        // Rotating
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 wantedForward = new Vector3(z, 0f, -x).normalized;
        charController.RotateInput(wantedForward);


        // Movement
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.LeftShift))
        {
            charController.StartMoving();
        }
        if (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.LeftShift))
        {
            charController.StopMoving();
        }


        // Push
        if ((Input.GetMouseButtonDown(1) || Input.GetButtonDown("Jump")))
        {
            charController.Push();
        }
    }

}
