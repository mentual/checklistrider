﻿using UnityEngine;
using static GameEnums;

public class BaseCharacterController : MonoBehaviour
{

    // // // Fields
    [Header("Base params")]
    public SpawnableCharactersType type;


    // // // Methods
    public virtual void SetTaskState(TaskStatusType state)
    {

    }

}
