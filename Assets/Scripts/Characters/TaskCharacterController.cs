﻿using UnityEngine;
using static GameEnums;

public class TaskCharacterController : BaseCharacterController
{

    // // // Fields
    // Hashes
    int armsParam = Animator.StringToHash("arms");
    [Header("Animator params")]
    [SerializeField] Animator animator;
    [SerializeField] int workingAnimNo;
    [SerializeField] int waitingWithTaskAnimNo;

    [Header("Particles")]
    [SerializeField] ParticleSystem flameParticle;
    [SerializeField] ParticleSystem smokeParticle;


    // // // Unity
    private void Start()
    {
        // Turn Off Particles in case they're not off in prefab
        TurnOffParticles();
    }


    // // // Methods
    public override void SetTaskState(TaskStatusType state)
    {
        switch (state)
        {
            case TaskStatusType.CURRENT:
                animator.SetInteger(armsParam, workingAnimNo);
                TurnOnAll();
                break;
            case TaskStatusType.DONE:
                animator.SetInteger(armsParam, workingAnimNo);
                TurnOffParticles();
                break;
            case TaskStatusType.WAITING:
                animator.SetInteger(armsParam, waitingWithTaskAnimNo);
                TurnOnFlame();
                break;
        }
    }

    void TurnOffParticles()
    {
        if (flameParticle.isPlaying)
            flameParticle.Stop();
        if (smokeParticle.isPlaying)
            smokeParticle.Stop();
    }

    void TurnOnFlame()
    {
        if (flameParticle.isStopped)
            flameParticle.Play();

        if (smokeParticle.isPlaying)
            smokeParticle.Stop();
    }

    void TurnOnAll()
    {
        if (flameParticle.isStopped)
            flameParticle.Play();
        if (smokeParticle.isStopped)
            smokeParticle.Play();
    }

}
