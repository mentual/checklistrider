﻿using Mentual.Patterns;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using static GameEnums;

public struct AudioParams
{
    public SoundType poolType;
    public GameAudioClip audioClip;
    public float playTime;
    public Vector3 position;
}

public class AudioManager : PersistentLazySingleton<AudioManager>
{

    // // // Fields
    public AudioDataSO audioData;
    Transform activeAudioParent;

    AudioSource melodySource;
    AudioSource drumSource;

    List<AudioSourcePool> pools = new List<AudioSourcePool>();

    bool isMuted;


    // // // Abstract Layer methods
    protected override void Init()
    {
        //Load AudioData
        audioData = GlobalManager.Instance.audioDataSO;

        // Check for MUTE
        Mute(PlayerData.Instance.GetAudioMutePref());

        //Create 'folder' for active audios |EZ scene ref/find
        activeAudioParent = new GameObject("ActiveAudioParent").transform;
        activeAudioParent.parent = transform;

        //Initialize music MELODY Source
        melodySource = gameObject.AddComponent<AudioSource>();
        melodySource.loop = true;
        melodySource.playOnAwake = true;
        melodySource.outputAudioMixerGroup = audioData.melodyMixer;

        //Initialize music DRUM Source
        drumSource = gameObject.AddComponent<AudioSource>();
        drumSource.loop = true;
        drumSource.playOnAwake = true;
        drumSource.outputAudioMixerGroup = audioData.drumMixer;
    }


    // // // Methods
    #region PoolHandling
    AudioSourcePool CreatePoolInstance(SoundType type)
    {
        var pool = audioData.audioSourcePools.Find(x => x.type == type);

        if (pool == null)
        {
            Debug.LogError("No AudioSourcePool found of type: " + type);
            return null;
        }
        else
        {
            // If we accidentially removed pool from the list?!!??
            // We should remove all 'null' instances, to drop REFS!
            pools.RemoveAll(item => item == null);

            var poolInstance = Instantiate(pool);
            poolInstance.transform.parent = transform;
            pools.Add(poolInstance);
            return poolInstance;
        }
    }

    AudioSourcePool GetPool(SoundType type)
    {
        var pool = pools.Find(x => x.type == type);

        if (pool == null)
            pool = CreatePoolInstance(type);

        return pool;
    }
    #endregion

    #region Playing Music
    public void PlayMusic(AudioClip melodyClip, AudioClip drumClip, float playTime = 0f)
    {
        melodySource.clip = melodyClip;
        drumSource.clip = drumClip;

        melodySource.time = playTime;
        drumSource.time = playTime;

        melodySource.Play();
        drumSource.Play();
    }

    public void StopMusic()
    {
        melodySource.Stop();
        melodySource.clip = null;
        drumSource.Stop();
        drumSource.clip = null;
    }

    public float GetMusicTime()
    {
        return melodySource.time;
    }
    #endregion

    #region Playing SFX
    /// <summary>
    /// Simple game... little sounds... 1 Playing/Creating method & it'll still work great...
    /// </summary>
    public PoolableAudio PlayAudio(AudioParams audioParams)
    {
        var chosenClip = audioParams.audioClip;

        if (chosenClip == null)
        {
            Debug.LogError("No clip loaded");
            return null;
        }

        var audioClip = chosenClip.GetPseudoRandomClip();

        if (audioClip == null)
            return null;

        float finalPlayTime = audioParams.playTime > 0f ? audioParams.playTime : audioClip.length;

        var poolableAudio = GetPool(chosenClip.audioPool).GetInstance();
        poolableAudio.transform.parent = activeAudioParent;

        poolableAudio.Play(audioClip, audioParams.position, finalPlayTime);

        return poolableAudio;
    }

    public void KillActiveAudio()
    {
        foreach (var audio in activeAudioParent.GetComponentsInChildren<PoolableAudio>())
        {
            audio.ReturnToPool();
        }
    }


    public void PlayGlassHit(Vector3 position)
    {
        var clipToPlay = audioData.glassHitClips;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = position,
        };

        PlayAudio(audioParams);
    }
    public void PlayConcreteHit(Vector3 position)
    {
        var clipToPlay = audioData.concreteHitClips;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = position,
        };

        PlayAudio(audioParams);
    }
    public void PlayDefaultHit(Vector3 position)
    {
        var clipToPlay = audioData.defaultHitClips;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = position,
        };

        PlayAudio(audioParams);
    }
    public void PlayTaskCompleted(Vector3 position)
    {
        var clipToPlay = audioData.taskCompleteClip;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = position,
        };

        PlayAudio(audioParams);
    }
    public void PlayGameOverJingle()
    {
        var clipToPlay = audioData.gameOver;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    public void PlayGameWinJingle()
    {
        var clipToPlay = audioData.gameWin;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    public void PlayTaskTabOpen()
    {
        var clipToPlay = audioData.taskScreenOpen;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    public void PlayTaskTabClose()
    {
        var clipToPlay = audioData.taskScreenClose;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    public void PlayUI_Play()
    {
        var clipToPlay = audioData.UI_Play;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    public void PlayUI_Restart()
    {
        var clipToPlay = audioData.UI_Restart;

        AudioParams audioParams = new AudioParams
        {
            audioClip = clipToPlay,
            poolType = clipToPlay.audioPool,
            position = Vector3.zero,
        };

        PlayAudio(audioParams);
    }
    #endregion


    #region MixerMethods
    AudioMixerSnapshot lastSnapshot;

    public void Mute(bool mute)
    {
        isMuted = mute;

        if (isMuted)
        {
            SetMuteSnapshot(0.01f);
        }
        else
        {
            SetLastSnapshot(0.01F);
        }
    }

    public void SetMuteSnapshot(float transitionTime)
    {
        audioData.muteSnapshot.TransitionTo(transitionTime);
    }

    public void SetNormalSnapshot(float transitionTime)
    {
        if (!isMuted)
        {
            audioData.startingScreenSnapshot.TransitionTo(transitionTime);
            lastSnapshot = audioData.startingScreenSnapshot;
        }
    }

    public void SetGamePlaySnapshot(float transitionTime)
    {
        if (!isMuted)
        {
            audioData.gameplaySnapshot.TransitionTo(transitionTime);
            lastSnapshot = audioData.gameplaySnapshot;
        }
    }

    public void SetPauseSnapshot(float transitionTime)
    {
        if (!isMuted)
        {
            audioData.pauseSnapshot.TransitionTo(transitionTime);
            lastSnapshot = audioData.pauseSnapshot;
        }
    }

    public void SetMasterVolume(float level)
    {
        if (!isMuted)
            audioData.masterMixer.SetFloat("MasterVol", level);
    }

    public void SetLastSnapshot(float transitionTime)
    {
        if (lastSnapshot == null)
            lastSnapshot = audioData.gameplaySnapshot;

        if (!isMuted)
            lastSnapshot.TransitionTo(transitionTime);
    }

    #endregion
}
