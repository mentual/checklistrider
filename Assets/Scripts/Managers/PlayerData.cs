﻿using Mentual.Patterns;
using UnityEngine;

public class PlayerData : PersistentLazySingleton<PlayerData>
{

    // // // Fields
    #region Fields
    // STATISTICS
    public const string STATS_RECORD = "StatsContainer";

    // SETTINGS
    public const string SETTINGS_MUTE = "SettingsMute";
    #endregion


    // // // Methods
    protected override void Init()
    {
    }

    #region Loading
    public int GetRecord()
    {
        return PlayerPrefs.GetInt(STATS_RECORD);
    }

    public bool GetAudioMutePref()
    {
        return PlayerPrefs.GetInt(SETTINGS_MUTE, 0) == 0 ? false : true;
    }
    #endregion


    #region Saving
    public void SaveRecord(int val)
    {
        PlayerPrefs.SetInt(STATS_RECORD, val);
    }

    public void SaveAudioMute(bool active)
    {
        PlayerPrefs.SetInt(SETTINGS_MUTE, active ? 1 : 0);
    }
    #endregion
}
