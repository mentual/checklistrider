﻿
using Mentual.Patterns;

public class GameDataManager : PersistentLazySingleton<GameDataManager>
{

    public GameDataSO gameData;

    protected override void Init()
    {
        gameData = GlobalManager.Instance.gameDataSO;
    }

}
