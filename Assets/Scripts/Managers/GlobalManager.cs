﻿using Mentual.Patterns;
using UnityEngine;

public class GlobalManager : PersistentLazySingleton<GlobalManager>
{

    public GameDataSO gameDataSO;
    public AudioDataSO audioDataSO;

    protected override void Init()
    {
        // App Settings
        Application.targetFrameRate = 60;
    }
}
