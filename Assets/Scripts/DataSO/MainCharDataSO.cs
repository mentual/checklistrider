﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MainCharData", menuName = "Data/MainCharData", order = 2)]
public class MainCharDataSO : ScriptableObject
{

    public MainCharController mainCharPrefab;
    [Header("Main Controller")]
    public float inputDelay;
    [Tooltip("In degrees")]
    public float maxRotationPerSecond;
    public float pushCD;

    [Header("Physics Controller")]
    public float moveStrengthMag;

    public float pushForceMag;
    public LayerMask pushLayers;
    public float pushDisntanceCheck;
    public float pushDelay;

}
