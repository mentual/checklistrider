﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "AudioData", menuName = "Data/Audio/AudioData", order = 2)]
public class AudioDataSO : ScriptableObject
{

    public AudioMixer masterMixer;
    public AudioMixerGroup masterMixerGroup;
    public AudioMixerGroup musicMixerGroup;
    public AudioMixerGroup melodyMixer;
    public AudioMixerGroup drumMixer;
    public AudioMixerGroup SFX_MixerGroup;

    public AudioMixerSnapshot muteSnapshot;
    public AudioMixerSnapshot startingScreenSnapshot;
    public AudioMixerSnapshot gameplaySnapshot;
    public AudioMixerSnapshot pauseSnapshot;

    public AudioClip melodyClip;
    public AudioClip drumClip;

    [Header("AudioSourcesPool")]
    public List<AudioSourcePool> audioSourcePools;

    [Header("Audio Data")]
    public GameAudioClip glassHitClips;
    public GameAudioClip concreteHitClips;
    public GameAudioClip defaultHitClips;
    public GameAudioClip taskCompleteClip;
    [Space(15)]
    public GameAudioClip gameWin;
    public GameAudioClip gameOver;
    public GameAudioClip taskScreenOpen;
    public GameAudioClip taskScreenClose;
    [Space(15)]
    public GameAudioClip UI_Play;
    public GameAudioClip UI_Restart;

}
