﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static GameEnums;

[CreateAssetMenu(fileName = "TaskData", menuName = "Data/TaskData", order = 6)]
public class TaskDataSO : ScriptableObject
{
    [Header("Tasks Data")]
    public int tasksToDo;
    public int selfTasksMin;
    public int programmerTasksMin;
    public int gdTasksMin;
    public int artistTasksMin;
    public int testerTaskMin;

    [Header("Tasks")]
    public List<TaskData> taskList;

}


[Serializable]
public class TaskData
{
    public string taskDescription;
    public CharacterTaskTypes taskType;
    public float timeToFinish;
}