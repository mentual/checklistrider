﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharactersData", menuName = "Data/CharactersData", order = 3)]
public class CharactersDataSO : ScriptableObject
{
    [Header("Character prefabs")]
    public List<BaseCharacterController> characterPrefabs;

    [Header("Numbers of NPCs")]
    [Tooltip("Including Synalendzix")]
    public int programmersCount;
    public int GDCount;
    [Tooltip("Including Ryszardzix & Roberdzix")]
    public int GraphicsCount;
    [Tooltip("!!!Excluding US!!!")]
    public int TestersCount;

    [Header("FOV Effector")]
    public FOV_EffectorValueObject fovEffectorData;

}
