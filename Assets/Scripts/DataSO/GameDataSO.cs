﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Data/GameData", order = 1)]
public class GameDataSO : ScriptableObject
{
    [Header("Main Data")]
    public float timeScalePlay;
    public float timeScaleTaskTab;
    public float timeScaleEnd;
    public float startingScreenTransitionIn;
    public float startingScreenTransitionOut;

    [Header("Clock")]
    public TimeData timeData;

    [Header("Ref Data")]
    public MainCharDataSO mainCharData;
    public CharactersDataSO charactersData;
    public TaskDataSO taskData;

    [Header("TaskBar")]
    public FillableBarData taskBarData;

    [Header("DeathBar")]
    public FillableBarData deathBarData;

}
