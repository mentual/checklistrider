﻿using UnityEngine;
using Mentual.UI;
using TMPro;


public class UI_HUD : UI_ScreenBase
{

    [SerializeField] FillableBar taskBar;
    [SerializeField] FillableBar deathBar;
    [SerializeField] TextMeshProUGUI clockText;
    [SerializeField] TextMeshProUGUI currentTaskText;

    public FillableBar TaskBar { get => taskBar; }
    public FillableBar DeathBar { get => deathBar; }
    public TextMeshProUGUI ClockText { get => clockText; }
    public TextMeshProUGUI CurrentTaskText { get => currentTaskText; }


}
