﻿using Mentual.UI;
using System.Collections.Generic;
using UnityEngine;

public class UI_TaskScreen : UI_ScreenBase
{

    // // // Fields
    [SerializeField] Transform taskContainer;
    [SerializeField] Task taskPrefab;
    [SerializeField] List<Task> taskList = new List<Task>();


    // // // Methods
    public void Reset()
    {
        foreach (var task in taskList)
        {
            Destroy(task.gameObject);
        }

        taskList.Clear();
    }

    public void FeedTasks(List<TaskData> tasks)
    {
        foreach (var task in tasks)
        {
            var taskInstance = Instantiate(taskPrefab, taskContainer);
            taskInstance.Init(task.taskDescription);

            taskList.Add(taskInstance);
        }
    }

    public void StartTask(int taskNo)
    {
        taskList[taskNo].StartTask();
    }

    public void CompleteTask(int taskNo, string timeCompleted)
    {
        taskList[taskNo].CompleteTask(timeCompleted);
    }

}
