﻿using Mentual.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_StartingScreen : UI_ScreenBase
{
    UIController controller;


    public void Init(UIController controller)
    {
        this.controller = controller;
    }


    public void Play()
    {
        controller.OnPressPlay?.Invoke();
    }
}
