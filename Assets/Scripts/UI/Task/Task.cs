﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static GameEnums;

public class Task : MonoBehaviour
{

    // // // Fields
    [SerializeField] TaskStatusType status;

    [SerializeField] TextMeshProUGUI description;
    [SerializeField] TextMeshProUGUI completeTimeText;

    [SerializeField] Image backgroundImage;
    [SerializeField] Image statusIcon;

    [SerializeField] Color waitingColor;
    [SerializeField] Color currentColor;
    [SerializeField] Color doneColor;


    // // // Methods
    public void Init(string description)
    {
        this.description.text = description;
        completeTimeText.text = string.Empty;

        status = TaskStatusType.WAITING;

        RefreshStatus();
    }

    public void StartTask()
    {
        status = TaskStatusType.CURRENT;

        RefreshStatus();
    }

    public void CompleteTask(string timeCompleted)
    {
        completeTimeText.text = timeCompleted;

        status = TaskStatusType.DONE;

        RefreshStatus();
    }

    void RefreshStatus()
    {
        switch (status)
        {
            case TaskStatusType.CURRENT:
                backgroundImage.color = currentColor;
                statusIcon.enabled = false;
                break;
            case TaskStatusType.DONE:
                backgroundImage.color = doneColor;
                statusIcon.enabled = true;
                break;
            case TaskStatusType.WAITING:
                backgroundImage.color = waitingColor;
                statusIcon.enabled = false;
                break;
        }
    }

}
