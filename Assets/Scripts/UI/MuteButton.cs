﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MuteButton : MonoBehaviour
{

    // // // Fields
    [SerializeField] Image image;
    [SerializeField] TextMeshProUGUI muteText;

    [SerializeField] Sprite soundSprite, mutedSprite;
    [SerializeField] string soundText, mutedText;


    // // // Unity
    private void Start()
    {
        CheckSprites();
        AudioManager.Instance.Mute(PlayerData.Instance.GetAudioMutePref());
    }


    // // // Methods
    private void CheckSprites()
    {
        if (PlayerData.Instance.GetAudioMutePref())
        {
            image.sprite = mutedSprite;
            muteText.text = mutedText;
        }
        else
        {
            image.sprite = soundSprite;
            muteText.text = soundText;
        }
    }

    public void ChangeMute()
    {
        PlayerData.Instance.SaveAudioMute(!PlayerData.Instance.GetAudioMutePref());

        AudioManager.Instance.Mute(PlayerData.Instance.GetAudioMutePref());

        CheckSprites();
    }

}
