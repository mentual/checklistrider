﻿using Mentual.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_EndScreen : UI_ScreenBase
{

    // // // Fields
    UIController controller;

    [Header("Bgs")]
    [SerializeField] GameObject winScreen;
    [SerializeField] GameObject gameOverScreen;
    [Header("times")]
    [SerializeField] TextMeshProUGUI topTimeText;
    [SerializeField] TextMeshProUGUI currentTimeText;
    [SerializeField] TextMeshProUGUI topTimeStat;
    [SerializeField] TextMeshProUGUI currentTimeStat;


    // // // Methods
    public void Init(UIController controller)
    {
        this.controller = controller;
    }

    public void SetWin()
    {
        SetScreenActive(true);

        winScreen.SetActive(true);
        gameOverScreen.SetActive(false);

        int currentTime = GameController.Instance.TimerController.GetCurrentTime();


        // HighScore
        int recordTime = PlayerData.Instance.GetRecord();
        topTimeText.gameObject.SetActive(true);
        topTimeStat.text = TimerController.SecondsToText(recordTime);

        // Current Score
        currentTimeText.gameObject.SetActive(true);
        currentTimeStat.text = TimerController.SecondsToText(currentTime);
    }

    public void SetGameOver()
    {
        SetScreenActive(true);

        winScreen.SetActive(false);
        gameOverScreen.SetActive(true);


        // HighScore
        int recordTime = PlayerData.Instance.GetRecord();

        if (recordTime != 0)
        {
            topTimeText.gameObject.SetActive(true);
            topTimeStat.text = TimerController.SecondsToText(recordTime);
        }
        else
        {
            topTimeText.gameObject.SetActive(false);
            topTimeStat.text = string.Empty;
        }

        // Current Score
        currentTimeText.gameObject.SetActive(false);
        currentTimeStat.text = string.Empty;
    }


    /// <summary>
    /// BUTTON REF!
    /// </summary>
    public void Reset()
    {
        controller.OnPressReset();
    }

}
