﻿using UnityEngine;
using UnityEngine.Events;

public class UIController : MonoBehaviour
{

    // // // Fields
    [Header("Self Refs")]
    [SerializeField] AudioListener uiListener;
    [SerializeField] Camera uiCamera;

    [Header("Screens")]
    [SerializeField] UI_StartingScreen startingScreen;
    [SerializeField] UI_TaskScreen taskScreen;
    [SerializeField] UI_EndScreen endScreen;
    [SerializeField] UI_HUD hud;
    public UI_HUD Hud { get => hud; }
    public UI_TaskScreen TaskScreen { get => taskScreen; }

    public UnityAction OnPressPlay;
    public UnityAction OnPressReset;



    // // // Unity
    private void Start()
    {
        SubscribeEvents();

        startingScreen.Init(this);
        endScreen.Init(this);

        SetTaskScreenActive(false);
        SetEndScreenActive(false);
    }

    // // // Methods
    void SubscribeEvents()
    {
        hud.DeathBar.OnDepleted += GameController.Instance.GameOver;
        hud.TaskBar.OnFilled += GameController.Instance.CompleteTask;
    }

    public void EnablePlay()
    {
        startingScreen.SetScreenInteractable(true);
    }

    public void PrepareStartScreen(bool enableInteractions)
    {
        taskScreen.SetScreenActive(false);
        endScreen.SetScreenActive(false);
        hud.SetScreenActive(false);

        startingScreen.SetScreenActive(true, GameDataManager.Instance.gameData.startingScreenTransitionIn);

        uiCamera.enabled = true;
        uiListener.enabled = true;

        startingScreen.SetScreenInteractable(enableInteractions);
    }

    public void GameStart()
    {
        taskScreen.SetScreenActive(false);
        endScreen.SetScreenActive(false);

        //Prepare HUD
        hud.SetScreenActive(true);
        hud.TaskBar.Init(GameDataManager.Instance.gameData.taskBarData);
        hud.DeathBar.Init(GameDataManager.Instance.gameData.deathBarData);

        startingScreen.SetScreenActive(false, GameDataManager.Instance.gameData.startingScreenTransitionOut);

        uiCamera.enabled = false;
        uiListener.enabled = false;

        startingScreen.SetScreenInteractable(true);
    }

    public void GameEnd(bool win)
    {
        startingScreen.SetScreenActive(false);
        taskScreen.SetScreenActive(false);
        hud.SetScreenActive(false);

        if (win)
        {
            endScreen.SetWin();
        }
        else
        {
            endScreen.SetGameOver();
        }
    }


    public void SetTaskScreenActive(bool active)
    {
        taskScreen.SetScreenActive(active);
    }

    public void SetEndScreenActive(bool active)
    {
        endScreen.SetScreenActive(active);
    }

    public void HurtPlayer(float amount)
    {
        hud.DeathBar.FillAmount(amount);
    }

    public void TaskProgress(float amount)
    {
        hud.TaskBar.FillAmount(amount);
    }

    public void CompleteTask()
    {
        hud.TaskBar.ResetBar();
    }


    public void SetMute()
    {

    }

}
