﻿using UnityEngine;
using Mentual.Patterns;
using static GameEnums;

public class PoolablePushParticles : GenericMonoBehaviourPoolableObject
{

    // // // Fields
    [Header("Refs")]
    [SerializeField] ParticleSystem pushParticles;

    [Header("Colors")]
    [SerializeField] Color defaultColor;
    [SerializeField] Color glassColor;
    [SerializeField] Color concreteColor;

    float timeout;
    float startTime;


    // // // Override
    public override void Prepare()
    {
        base.Prepare();

        startTime = Time.time;

        timeout = pushParticles.main.duration;
    }


    // // // Unity
    void Update()
    {
        // Waiting for timeout
        if (Time.time - startTime > timeout)
        {
            ReturnToPool();
        }
    }


    // // // Methods
    public void Init(Vector3 position, ObstaclesTypes obstacleType)
    {
        // Shall be in worldSpace
        transform.parent = null;

        transform.position = position;

        var main = pushParticles.main;

        switch (obstacleType)
        {
            case ObstaclesTypes.DEFAULT:
                main.startColor = defaultColor;
                break;
            case ObstaclesTypes.GLASS:
                main.startColor = glassColor;
                break;
            case ObstaclesTypes.CONCRETE:
                main.startColor = concreteColor;
                break;
        }

        pushParticles.Play();

    }
}
