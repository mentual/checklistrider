﻿using Mentual.Patterns;

public class PushParticlePool : GenericMonoBehaviourObjectPool<PoolablePushParticles>
{
    private void OnDestroy()
    {
        foreach (var liveParticle in FindObjectsOfType<PoolablePushParticles>())
        {
            Destroy(liveParticle.gameObject);
        }
    }
}
