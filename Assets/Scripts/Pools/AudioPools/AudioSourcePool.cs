﻿using Mentual.Patterns;
using static GameEnums;

public class AudioSourcePool : GenericMonoBehaviourObjectPool<PoolableAudio>
{
    public SoundType type;
}
