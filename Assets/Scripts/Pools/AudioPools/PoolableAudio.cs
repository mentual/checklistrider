﻿using UnityEngine;
using Mentual.Patterns;

public class PoolableAudio : GenericMonoBehaviourPoolableObject
{

    // // // Fields
    [SerializeField] AudioSource source;
    [SerializeField] bool forceStopAfterTimout;

    //target is used for our Audio to TRACK it (much more flexible than parenting the object)
    Transform target;

    float timeout;
    float startTime;


    // // // Override
    public override void Prepare()
    {
        base.Prepare();

        target = null;
        startTime = Time.time;
    }

    public override void ReturnToPool()
    {
        base.ReturnToPool();

        target = null;
    }


    // // // Unity
    void Update()
    {
        // only if source is not in loop mode
        if (!source.loop || forceStopAfterTimout)
        {
            // Waiting for timeout
            if (Time.time - startTime > timeout)
            {
                ReturnToPool();
            }
        }

        if (target != null)
        {
            transform.position = target.position;
        }
    }


    // // // Methods
    public void PlayOneShot(AudioClip clip, Vector3 position)
    {
        Play(clip, position, clip.length);
    }

    public void Play(AudioClip clip, Vector3 position, float playTime)
    {
        timeout = playTime;

        transform.position = position;

        source.clip = clip;
        source.Play();
    }

    public void Play(AudioClip clip, Vector3 position, Transform newParent, float playTime)
    {
        timeout = playTime;

        transform.position = position;
        target = newParent;

        source.clip = clip;
        source.Play();
    }


    public void SetForceStopAfterTimout(bool forceStopAfterTimout)
    {
        this.forceStopAfterTimout = forceStopAfterTimout;
    }
}
