﻿using System.Collections.Generic;
using UnityEngine;
using static GameEnums;

[System.Serializable]
[CreateAssetMenu(fileName = "AudioClipsData", menuName = "Data/Audio/AudioClipsData", order = 9)]
public class GameAudioClip : ScriptableObject
{

    [Tooltip("Which pool from DATA will be used")]
    public SoundType audioPool;
    public AudioClip[] clips;

    // NON SERIALIZED FOR in-object randomization!
    [System.NonSerialized]
    int lastClipNo = -1;
    [System.NonSerialized]
    List<int> pseudoClipIDs = new List<int>();


    // // // Methods

    /// <summary>
    /// Method for 'forced' returning. No null/empty array checks here
    /// Works great for previously randomized/approved clips
    /// where no additional checks are needed
    /// </summary>
    AudioClip GetClipNoChecks(int clipNo)
    {
        // ref last clip (can be used LATER on)
        lastClipNo = clipNo;

        // return the clip
        return clips[clipNo];
    }

    public AudioClip GetClip(int clipNo)
    {
        // Initial checks
        if (clipNo >= clips.Length)
        {
            Debug.LogErrorFormat("Trying to get clip of no: {0}, from a list of max: {2}", clipNo, clips.Length);
            return null;
        }

        // return the clip
        return GetClipNoChecks(clipNo);
    }

    /// <summary>
    /// Never get the same clip twice in a row
    /// | If only there are >2 clips!
    /// </summary>
    public AudioClip GetPseudoRandomClip()
    {
        // Initial checks for zero/one entry array
        if (clips.Length == 0)
        {
            return null;
        }
        else if (clips.Length == 1)
        {
            return GetClipNoChecks(0);
        }


        // Check and initialize pseudoIDs list
        if (pseudoClipIDs.Count == 0)
        {
            for (int i = 0; i < clips.Length; i++)
            {
                pseudoClipIDs.Add(i);
            }

            pseudoClipIDs.Remove(lastClipNo);
        }

        // Randomize & return proper clip
        int random = Random.Range(0, pseudoClipIDs.Count);
        int clipNo = pseudoClipIDs[random];
        pseudoClipIDs.Remove(clipNo);
        return GetClipNoChecks(clipNo);
    }

    public AudioClip GetRandomClip()
    {
        // Initial checks for zero/one entry array
        if (clips.Length == 0)
        {
            return null;
        }
        else if (clips.Length == 1)
        {
            return GetClipNoChecks(0);
        }


        // Randomize & return proper clip
        int random = Random.Range(0, clips.Length);
        return GetClipNoChecks(random);
    }

}
