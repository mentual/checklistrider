﻿using Mentual.Patterns;

public abstract class AI_StateBase : BaseState
{

    public new AI_ControllerBase owner;


    public override void PrepareState()
    {
        base.PrepareState();

        owner = base.owner.GetComponent<AI_ControllerBase>();
    }

}
