﻿using UnityEngine;

public class AI_IdleState : AI_StateBase
{

    // // // Fields
    float stopTime;


    // // // Abstract Layer
    public override void PrepareState()
    {
        base.PrepareState();

        owner.StopAgent();

        stopTime = Time.time + owner.data.recoveryTime;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (stopTime < Time.time)
        {
            owner.ChangeState(new AI_ChaseState());
        }
    }
}
