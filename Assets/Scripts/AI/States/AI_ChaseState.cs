﻿using UnityEngine;

public class AI_ChaseState : AI_StateBase
{

    public override void FinishState()
    {
        base.FinishState();
    }

    public override void PrepareState()
    {
        base.PrepareState();

        owner.StartAgent();
    }

    public override void UpdateState()
    {
        if (Time.frameCount % owner.data.framesToUpdateAgent == 0)
        {
            owner.UpdateAgent();

            if (owner.RecoveryCheck())
                owner.ChangeState(new AI_IdleState());
        }

    }
}
