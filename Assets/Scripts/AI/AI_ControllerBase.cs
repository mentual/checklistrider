﻿using Mentual.Patterns;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class AI_ControllerBase : BaseStateMachine
{

    // // // Fields
    [Header("Data ref")]
    public AIDataSO data;
    [SerializeField] protected FoVEffector fovEffector;
    [Header("Anims")]
    [SerializeField] protected Animator animator;

    int legsAnimHash = Animator.StringToHash("legs");
    int armsAnimHash = Animator.StringToHash("arms");

    //Priv
    protected List<ICheckpointable> checkpoints = new List<ICheckpointable>();
    protected NavMeshAgent agent;


    // // // Unity
    protected virtual void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    protected virtual void Start()
    {
        FindCheckPoints();

        fovEffector.Init(data.fovEffectorData, GameController.Instance.CharController.MainChar.transform);
        fovEffector.OnTargetInRange += HurtPlayer;

        //Starting with an Idle State
        ChangeState(new AI_IdleState());
    }


    // // // Abstract Layer
    public abstract void FindCheckPoints();
    public abstract void UpdateAgent();
    public abstract bool RecoveryCheck();

    public virtual void ChooseCheckPoint()
    {
        int random = Random.Range(0, checkpoints.Count);
        agent.destination = checkpoints[random].GetDestination();
    }

    public virtual void StopAgent()
    {
        agent.enabled = false;
        animator.SetInteger(armsAnimHash, data.recoveryAnimArms);
        animator.SetInteger(legsAnimHash, data.recoveryAnimLegs);
    }

    public virtual void StartAgent()
    {
        agent.enabled = true;
        animator.SetInteger(legsAnimHash, data.activeAnimLegs);
        animator.SetInteger(armsAnimHash, data.activeAnimArms);
    }

    public void HurtPlayer()
    {
        GameController.Instance.HurtPlayer(-Time.deltaTime * data.chasingMult);
    }

}
