﻿using UnityEngine;

public interface ICheckpointable
{
    Vector3 GetDestination();
}
