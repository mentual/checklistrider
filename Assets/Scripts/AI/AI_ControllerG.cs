﻿public class AI_ControllerG : AI_ControllerBase
{

    // // // Abstract Layer
    public override void FindCheckPoints()
    {
        var checkpointsOnScene = FindObjectsOfType<AI_Checkpoint>();

        foreach (var check in checkpointsOnScene)
        {
            checkpoints.Add(check);
        }
    }

    public override bool RecoveryCheck()
    {
        // Check if we've reached the destination
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude < 0.01f)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public override void StartAgent()
    {
        base.StartAgent();

        ChooseCheckPoint();
    }

    public override void UpdateAgent()
    {
    }
    public void Deactivate()
    {
        fovEffector.enabled = false;
        agent.speed = 0.01f;
    }

}
