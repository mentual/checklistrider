﻿using UnityEngine;

public class AI_ControllerD : AI_ControllerBase
{

    // // // Fields
    float currentLookingTime;


    // // // Unity
    protected override void Update()
    {
        base.Update();

        currentLookingTime += Time.deltaTime;
    }


    // // // Abstract Layer
    public override void FindCheckPoints()
    {
        var checkpointsOnScene = FindObjectsOfType<MainCharController>();

        foreach (var check in checkpointsOnScene)
        {
            checkpoints.Add(check);
        }
    }

    public override void UpdateAgent()
    {
        ChooseCheckPoint();
    }

    public override bool RecoveryCheck()
    {
        return currentLookingTime > data.chasingTime;
    }

    public override void StartAgent()
    {
        base.StartAgent();
        currentLookingTime = 0f;
    }

    public void Deactivate()
    {
        fovEffector.enabled = false;
        agent.speed = 0.01f;
    }

}
