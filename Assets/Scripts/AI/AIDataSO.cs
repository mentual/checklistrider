﻿using UnityEngine;

[CreateAssetMenu(fileName = "AIData", menuName = "Data/AIData", order = 4)]
public class AIDataSO : ScriptableObject
{

    [Header("Base Params")]
    public int framesToUpdateAgent;
    public float recoveryTime;
    public float chasingMult;

    [Header("FOV Effector")]
    public FOV_EffectorValueObject fovEffectorData;

    [Header("Anims")]
    public int activeAnimArms;
    public int activeAnimLegs;
    public int recoveryAnimArms;
    public int recoveryAnimLegs;

    /// <summary>
    /// Time only for characters which will stop when chasingTime is achieved
    /// </summary>
    [Header("Unique Params")]
    public float chasingTime;

}
