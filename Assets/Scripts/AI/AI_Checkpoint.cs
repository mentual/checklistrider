﻿using UnityEngine;

public class AI_Checkpoint : MonoBehaviour, ICheckpointable
{
    public Vector3 GetDestination()
    {
        return transform.position;
    }
}
