﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct FOV_EffectorValueObject
{
    [Tooltip("This is offset from local Transform ... So v3.zero is most likely to be proper")]
    public Vector3 origin;
    public float startingAngle;
    public float fov;
    public float viewDistance;
    [Tooltip("how many ray/future edges of FOV mesh")]
    public int rayCount;

    public LayerMask obstacleLayerMask;
}


[RequireComponent(typeof(MeshFilter))]
public class FoVEffector : MonoBehaviour
{

    // // // Fields
    [SerializeField] bool rangeColorChange;
    [SerializeField] Color rangeColor;
    [SerializeField] Color normalColor;

    bool isTargetInRange;
    public bool IsTargetInRange { get => isTargetInRange; }

    FOV_EffectorValueObject data;

    Transform target;
    Mesh mesh;
    Renderer rend;

    // Mesh
    Vector3[] vertices;
    Vector2[] uv;
    int[] triangles;

    // Actions
    public UnityAction OnTargetInRange;

    // Hashes
    int colorPropHash = Shader.PropertyToID("_BaseColor");


    // // // Unity
    private void Awake()
    {
        rend = GetComponent<Renderer>(); // Material is instantiating itself now... do not need to create new material
    }

    private void Start()
    {
        CreateMesh();
    }

    private void LateUpdate()
    {
        UpdateMesh();

        isTargetInRange = CheckTargetVisibility();

        if (rangeColorChange) // Material is instantiating itself now... do not need to create new material
        {
            if (isTargetInRange)
            {
                rend.material.SetColor(colorPropHash, rangeColor);
            }
            else
            {
                rend.material.SetColor(colorPropHash, normalColor);
            }
        }
    }


    // // // Methods
    public void Init(FOV_EffectorValueObject fovData, Transform target)
    {
        this.data = fovData;
        this.target = target;

        vertices = new Vector3[1 + data.rayCount + 1]; // ray 0 + ray origin
        Vector2[] uv = new Vector2[vertices.Length];
        triangles = new int[data.rayCount * 3];
        vertices[0] = data.origin;
    }

    void CreateMesh()
    {
        mesh = new Mesh();

        GetComponent<MeshFilter>().mesh = mesh;
    }

    void UpdateMesh()
    {
        float currentAngle = data.startingAngle;
        float angleIncrease = data.fov / data.rayCount;

        int vertexIndex = 1;
        int triangleIndex = 0;

        for (int i = 0; i <= data.rayCount; i++)
        {
            Vector3 vortex;

            Quaternion rotation = Quaternion.AngleAxis(currentAngle, Vector3.up);

            Vector3 rotateVectorForRay = rotation * transform.forward; // Raycast through transform forward

            Vector3 rotateVectorForMesh = rotation * Vector3.forward; // Mesh always global axis


            if (Physics.Raycast(transform.position, rotateVectorForRay, out RaycastHit raycast, data.viewDistance, data.obstacleLayerMask))
            {
                vortex = data.origin + rotateVectorForMesh * raycast.distance;
            }
            else
            {
                vortex = data.origin + rotateVectorForMesh * data.viewDistance;
            }

            vertices[vertexIndex] = vortex;

            if (i > 0)
            {
                triangles[triangleIndex + 0] = 0;
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }

            vertexIndex++;
            currentAngle += angleIncrease;
        }

        mesh.SetVertices(vertices);
        mesh.SetUVs(0, uv);
        mesh.SetTriangles(triangles, 0);
    }

    public bool CheckTargetVisibility()
    {
        // Target Check
        if (target == null)
            return false;


        // Angle Check
        var forwardToTargetAngle = Vector3.Angle(target.position - transform.position, transform.forward);
        bool angleCondition = forwardToTargetAngle < data.fov / 2f;

        if (!angleCondition)
            return false;


        // Distance Check
        var distance = (target.position - transform.position).magnitude;
        bool distanceCondition = distance < data.viewDistance;

        if (!distanceCondition)
            return false;


        // Raycast condition check -> If any obstacle is hit (anything on obstacleLayre -> RETURN)
        if (Physics.Raycast(transform.position, target.position - transform.position, distance, data.obstacleLayerMask))
            return false;


        //If all conditions are MET -> target is in VISIBLE RANGE
        OnTargetInRange?.Invoke();
        return true;

    }

}
