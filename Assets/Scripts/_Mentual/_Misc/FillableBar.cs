﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public struct FillableBarData
{
    [Header("Base")]
    [Tooltip("Max Value for fill")]
    public float maxValue;
    public float startingFill;

    [Header("Auto-Fill")]
    public bool autoFill;
    [Tooltip("Value per second")]
    public float autoFillVal;
}

public class FillableBar : MonoBehaviour
{

    // // // Fields
    [SerializeField] Image fillableImage;
    FillableBarData barData;

    // props
    bool isFilled;
    bool isDepleted;
    public bool IsFilled { get => isFilled; }
    public bool IsDepleted { get => isDepleted; }

    // Events
    public UnityAction OnFilled;
    public UnityAction OnDepleted;

    // Editor only for Tests
#if UNITY_EDITOR
    public FillableBarData BarData { get => barData; set => barData = value; }
    public Image FillableImage { get => fillableImage; set => fillableImage = value; }
#endif


    // // // Methods
    private void Update()
    {
        if (!barData.autoFill)
            return;

        FillAmount(barData.autoFillVal * Time.deltaTime);
    }


    // // // Methods
    /// <param name="amount">Just amount</param>
    /// <param name="fillCheck">Check if want to receive fill events (OnFill/OnDepleted)</param>
    public void Init(FillableBarData barData)
    {
        this.barData = barData;
        ResetBar();
    }

    public void FillAmount(float amount, bool fillCheck = true)
    {
        // Fill Bar
        float wantedFillAmount = fillableImage.fillAmount + amount / barData.maxValue;
        float newFillAmount = Mathf.Clamp01(wantedFillAmount);

        fillableImage.fillAmount = newFillAmount;

        // Check fill amount
        if (fillCheck)
            CheckFillValue(wantedFillAmount);
    }

    /// <param name="fillCheck">Check if want to receive fill events (OnFill/OnDepleted)</param>
    public void ResetBar(bool fillCheck = false)
    {
        // Reset
        fillableImage.fillAmount = 0f;

        //Fill with starting Value w/o checking fill (no event triggers)
        FillAmount(barData.startingFill, fillCheck);
    }

    void CheckFillValue(float wantedFillAmount)
    {
        if (wantedFillAmount >= 1f)
        {
            if (!isFilled)
            {
                OnFilled?.Invoke();
                isFilled = true;
            }
        }
        else
        {
            isFilled = false;
        }

        if (wantedFillAmount <= 0f)
        {
            if (!isDepleted)
            {
                OnDepleted?.Invoke();
                isDepleted = true;
            }
        }
        else
        {
            isDepleted = false;
        }
    }

}
