﻿/// <summary>
/// Base abstract class for
/// State Machine Pattern
/// </summary>

namespace Mentual.Patterns
{
    public abstract class BaseState
    {

        // // //Fields
        public BaseStateMachine owner;


        // // //Methods - virutal -> no need to implement them
        public virtual void PrepareState()
        {

        }

        public virtual void UpdateState()
        {

        }

        public virtual void FinishState()
        {

        }

    }
}
