﻿using UnityEngine;

namespace Mentual.Patterns
{
    public abstract class BaseStateMachine : MonoBehaviour
    {

        //Fields
        private BaseState currentState;


        // // // Unity
        protected virtual void Update()
        {
            if (currentState != null)
            {
                currentState.UpdateState();
            }
        }


        // // // Methods

        /// <summary>
        /// Method used to switch between states
        /// </summary>
        /// <param name="newState">NewState here. Can be null!</param>
        public void ChangeState(BaseState newState)
        {
            // If current state is active, finish IT!
            if (currentState != null)
            {
                currentState.FinishState();
            }

            currentState = newState;

            if (currentState != null)
            {
                //If newState is not null, ref owner
                currentState.owner = this;

                //If newState is not null, prepare it to work
                currentState.PrepareState();
            }
        }

    }
}
