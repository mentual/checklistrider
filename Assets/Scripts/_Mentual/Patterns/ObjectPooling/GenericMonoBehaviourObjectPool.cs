﻿using System.Collections.Generic;
using UnityEngine;

namespace Mentual.Patterns
{
    public class GenericMonoBehaviourObjectPool<T> : MonoBehaviour, IObjectPool<T> where T : MonoBehaviour, IPoolableObject
    {

        // // // Fields
        [SerializeField] T prefab;
        [SerializeField] int instancesToPrepare;

        Stack<T> instancesPool = new Stack<T>();


        // // // Unity
        private void Awake()
        {
            PrepareInitialInstances();
        }


        // // // Methods
        void PrepareInitialInstances()
        {
            for (int i = 0; i < instancesToPrepare; i++)
            {
                var instance = Instantiate(prefab);
                instance.ParentOrigin = this;
                instance.ReturnToPool();
            }
        }

        public T GetInstance()
        {
            T instance;

            if (instancesPool.Count > 0)
            {
                instance = instancesPool.Pop();

                instance.gameObject.SetActive(true);
            }
            else
            {
                instance = Instantiate(prefab);
            }

            instance.ParentOrigin = this;

            instance.Prepare();

            return instance;
        }

        public void ReturnToPool(T instance)
        {
            instance.gameObject.SetActive(false);

            // reset transform
            instance.transform.SetParent(transform);
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;
            instance.transform.localEulerAngles = Vector3.one;

            instancesPool.Push(instance);
        }

        public void ReturnToPool(object instance)
        {
            if (instance is T)
            {
                ReturnToPool((T)instance);
            }
        }

    }
}
