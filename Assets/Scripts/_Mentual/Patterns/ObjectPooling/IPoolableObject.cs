﻿namespace Mentual.Patterns
{
    public interface IPoolableObject : IParentableWithOrigin<IObjectPool>
    {
        void Prepare();
        void ReturnToPool();
    }

}