﻿namespace Mentual.Patterns
{
    public interface IObjectPool
    {
        void ReturnToPool(object instance);
    }

    public interface IObjectPool<T> : IObjectPool where T : IPoolableObject
    {
        T GetInstance();
        void ReturnToPool(T instance);
    }

}