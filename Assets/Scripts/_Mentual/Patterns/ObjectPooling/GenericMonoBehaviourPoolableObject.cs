﻿using UnityEngine;

namespace Mentual.Patterns
{
    public class GenericMonoBehaviourPoolableObject : MonoBehaviour, IPoolableObject
    {

        // // // Fields
        public IObjectPool ParentOrigin { get; set; }


        // // // Methods
        public virtual void Prepare()
        {
        }

        public virtual void ReturnToPool()
        {
            ParentOrigin.ReturnToPool(this);
        }

    }
}
