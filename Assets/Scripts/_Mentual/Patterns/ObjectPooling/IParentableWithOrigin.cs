﻿namespace Mentual.Patterns
{
    public interface IParentableWithOrigin<T>
    {
        T ParentOrigin { get; set; }
    }

}