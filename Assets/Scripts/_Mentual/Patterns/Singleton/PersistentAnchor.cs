﻿using UnityEngine;

namespace Mentual.Patterns
{
    public class PersistentAnchor : MonoBehaviour
    {

        // // // Fields
        static bool initialized;
        static Transform anchor;


        // // // Unity
        public static Transform GetAnchor()
        {
            if (!initialized)
                Create();
            return anchor;
        }

        static void Create()
        {
            GameObject go = new GameObject("PersistentAnchor");
            DontDestroyOnLoad(go);
            anchor = go.transform;
            initialized = true;
        }

    }
}
