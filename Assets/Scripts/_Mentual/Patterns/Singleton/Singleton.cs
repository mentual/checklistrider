﻿using UnityEngine;

namespace Mentual.Patterns
{
/// <summary>
/// Pattern for on-scene pre-created objects that need to be 'one and only' Instance present
/// </summary>
public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{

    // // // Fields
    private static T instance;
    public static T Instance
    {
        get
        {
            return instance;
        }
    }


    // // // Unity
    public virtual void Awake()
    {
        if (instance == null)
        {
            instance = (T)this;
        }
        else if (instance != this) //check if instance already exists (eg. duplicated/reloading scene...)
        {
            Destroy(gameObject);
        }
    }


    // // // Methods
    #region Utils
    static string GetName()
    {
        return typeof(T).Name;
    }

    protected static string GetTag()
    {
        return string.Format("{0}: ", GetName());
    }
        #endregion

    }
}
