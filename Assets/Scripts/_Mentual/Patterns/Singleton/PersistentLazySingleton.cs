﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mentual.Patterns
{
    /// <summary>
    /// MULTI-PURPOSE singleton pattern
    /// Pattern for creating lazy-singletons (on-the-fly)
    /// ! Adjusted to work on scene-present object & more !
    /// </summary>
    public abstract class PersistentLazySingleton<T> : MonoBehaviour where T : PersistentLazySingleton<T>
    {

        // // // Fields
        [SerializeField] protected bool persistent = true; //Can change if needed

        private static T instance;
        public static T Instance
        {
            get
            {
                Create();

                return instance;
            }
        }


        // // // Unity
        public virtual void Awake()
        {
            if (instance == null) // check if instance already exists
            {
                instance = (T)this;

                SelfInit(persistent);
            }
            else if (instance != this) // else check if current instance is this object (eg. duplicated/RE-loading scene...). Destroy if not
            {
                Destroy(gameObject);
            }
        }


        // // // Methods

        /// <summary>
        /// main creating method
        /// </summary>
        /// <param name="persistent">Check if want to persist through scenes</param>
        public static void Create(bool persistent = true)
        {
            if (!instance)
            {
                //find existing instance
                instance = FindObjectOfType<T>();

                if (instance != null)
                {
                    SelfInit(persistent);
                }
                else
                {
                    GameObject newObj = new GameObject(GetName());
                    //Adding component will immediately trigger AWAKE (with SelfInit etc)
                    newObj.AddComponent<T>();

                    //It is creating in persistent Anchor (on DontDestroyOnLoad Scene) -> need to move it then to 'normal' scene
                    if (!persistent)
                    {
                        instance.persistent = persistent;
                        instance.transform.SetParent(null);
                        SceneManager.MoveGameObjectToScene(instance.gameObject, SceneManager.GetActiveScene());
                    }
                }
            }
        }

        static void SelfInit(bool persistent = true)
        {
            if (persistent)
                SetParent(PersistentAnchor.GetAnchor());

            instance.Init();

            Debug.Log(GetTag() + "initialized");
        }

        /// <summary>
        /// All initialization here
        /// </summary>
        protected abstract void Init();


        #region Utils
        public static void SetParent(Transform newParent)
        {
            instance.transform.SetParent(newParent);
        }

        /// <summary>
        /// If need to check if singleton has instance
        /// *Can be null while ref to it while destroying/creating...
        /// ** Works much better than OnDestroy handling + returning null
        /// </summary>
        public static bool HasInstance()
        {
            return instance != null;
        }

        static string GetName()
        {
            return typeof(T).Name;
        }

        protected static string GetTag()
        {
            return string.Format("{0}: ", GetName());
        }
        #endregion
    }
}
