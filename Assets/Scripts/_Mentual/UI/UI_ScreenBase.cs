﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Mentual.UI
{
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(GraphicRaycaster))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UI_ScreenBase : MonoBehaviour
    {

        // // // Fields
        Canvas canvas;
        CanvasGroup canvasGroup;
        GraphicRaycaster raycaster;

        Coroutine blendingCoro;


        // // // Unity
        protected virtual void Awake()
        {
            canvas = GetComponent<Canvas>();
            canvasGroup = GetComponent<CanvasGroup>();
            raycaster = GetComponent<GraphicRaycaster>();
        }


        // // // Methods
        /// <summary>
        /// Fully means even canvas * group will be disabled
        /// </summary>
        public void SetScreenFullyActive(bool active)
        {
            gameObject.SetActive(active);

            canvas.enabled = active;

            raycaster.enabled = active;

            canvasGroup.enabled = active;
            canvasGroup.interactable = active;
            canvasGroup.blocksRaycasts = active;
            canvasGroup.alpha = active ? 1f : 0f;
        }

        /// <summary>
        /// It's canvas & canvasGroup will NOT be disabled
        /// It's enough for batches/setPassCalls ~ most optimization
        /// </summary>
        public void SetScreenActive(bool active)
        {
            // Coro can be active -> STOP IT
            if (blendingCoro != null)
                StopCoroutine(blendingCoro);

            raycaster.enabled = active;

            canvasGroup.interactable = active;
            canvasGroup.blocksRaycasts = active;
            canvasGroup.alpha = active ? 1f : 0f;
        }

        public void SetScreenActive(bool active, float blendTime)
        {
            raycaster.enabled = active;

            canvasGroup.interactable = active;
            canvasGroup.blocksRaycasts = active;
            var wantedStartAlpha = active ? 0f : 1f;
            var wantedEndAlpha = active ? 1f : 0f;
            BlendGroupAlpha(wantedStartAlpha, wantedEndAlpha, blendTime);
        }

        public void SetScreenInteractable(bool interactable)
        {
            canvasGroup.interactable = interactable;
        }

        public void BlendGroupAlpha(float from, float to, float blendTime)
        {
            if (blendingCoro != null)
                StopCoroutine(blendingCoro);

            blendingCoro = StartCoroutine(GroupBlendingC(from, to, blendTime));
        }

        IEnumerator GroupBlendingC(float from, float to, float blendTime)
        {
            float timer = 0f;

            canvasGroup.alpha = from;
            float currentAlpha = canvasGroup.alpha;

            while (timer < blendTime)
            {
                float t = timer / blendTime;

                float newAlpha = Mathf.Lerp(currentAlpha, to, t);

                canvasGroup.alpha = newAlpha;

                timer += Time.deltaTime;

                yield return null;
            }

            canvasGroup.alpha = to;
        }

    }

}

