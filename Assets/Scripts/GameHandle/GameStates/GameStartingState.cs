﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStartingState : GameStateBase
{
    public override void PrepareState()
    {
        base.PrepareState();

        AudioManager.Instance.SetNormalSnapshot(0.01f);

        Time.timeScale = 1f;

        AudioManager.Instance.PlayMusic(AudioManager.Instance.audioData.melodyClip, AudioManager.Instance.audioData.drumClip);

        owner.CharController.ResetChars();
        owner.UiController.PrepareStartScreen(SceneManager.GetSceneByBuildIndex(1).isLoaded);
    }
}
