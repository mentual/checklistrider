﻿using UnityEngine;

public class GameWinState : GameStateBase
{
    public override void PrepareState()
    {
        base.PrepareState();

        AudioManager.Instance.SetPauseSnapshot(0.01f);

        AudioManager.Instance.KillActiveAudio();
        AudioManager.Instance.StopMusic();
        AudioManager.Instance.PlayGameWinJingle();

        Time.timeScale = GameDataManager.Instance.gameData.timeScaleEnd;

        owner.UiController.GameEnd(true);
        owner.TimerController.ResetTimer();

        // Disable Dynamic Chars
        owner.CharController.DeactivateChars();
    }
}
