﻿using Mentual.Patterns;

public class GameStateBase : BaseState
{
    public new GameController owner;


    public override void PrepareState()
    {
        base.PrepareState();

        owner = base.owner.GetComponent<GameController>();
    }

}
