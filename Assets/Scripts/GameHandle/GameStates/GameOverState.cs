﻿using UnityEngine;

public class GameOverState : GameStateBase
{
    public override void PrepareState()
    {
        base.PrepareState();

        AudioManager.Instance.SetPauseSnapshot(0.01f);

        AudioManager.Instance.KillActiveAudio();
        AudioManager.Instance.StopMusic();
        AudioManager.Instance.PlayGameOverJingle();

        Time.timeScale = GameDataManager.Instance.gameData.timeScaleEnd;

        owner.UiController.GameEnd(false);

        owner.TimerController.ResetTimer();

        // Disable Dynamic Chars
        owner.CharController.DeactivateChars();
    }
}
