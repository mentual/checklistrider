﻿using UnityEngine;

public class GamePlayingState : GameStateBase
{

    public override void PrepareState()
    {
        base.PrepareState();

        AudioManager.Instance.SetGamePlaySnapshot(0.01f);

        Time.timeScale = GameDataManager.Instance.gameData.timeScalePlay;

        owner.UiController.GameStart();

        owner.TimerController.InitTimer(GameDataManager.Instance.gameData.timeData, owner.UiController.Hud.ClockText);

        //Spawn all characters after the LevelScene is loaded
        owner.CharController.InitCharacters(GameDataManager.Instance.gameData.charactersData.TestersCount, GameDataManager.Instance.gameData.charactersData.programmersCount, GameDataManager.Instance.gameData.charactersData.GraphicsCount, GameDataManager.Instance.gameData.charactersData.GDCount);

        // Init tasks
        owner.TaskController.CreateTasks();
        owner.TaskController.StartTask();

        // Disable Player Input
        owner.CharController.MainChar.SetInputActive(false);
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            owner.ShowTasks();
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            owner.HideTasks();
        }

#if PROGRAMMER_DEBUG
        if (Input.GetKeyDown(KeyCode.P))
        {
            owner.WinGame();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            owner.GameOver();
        }
#endif

    }

}
