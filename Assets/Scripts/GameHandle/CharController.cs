﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static GameEnums;

public class CharController : MonoBehaviour
{

    // // // Fields
    MainCharController mainChar;
    List<WorkplaceController> workplaces = new List<WorkplaceController>();
    List<BaseCharacterController> characters;
    // Props
    public MainCharController MainChar { get => mainChar; }
    public List<WorkplaceController> Workplaces { get => workplaces; }
    public List<BaseCharacterController> Characters { get => characters; set => characters = value; }


    // // // Methods
    public void InitCharacters(int testersCount, int programmersCount, int graphicsCount, int gameDesignerCount)
    {
        workplaces = FindObjectsOfType<WorkplaceController>().ToList();

        characters = new List<BaseCharacterController>();

        // Init Uniques... 100% always
        // Main Char
        FindAndInitNPCsOfType(SpawnableCharactersType.MAIN_CHAR, 1);
        mainChar = characters.Find(x => x.type == SpawnableCharactersType.MAIN_CHAR).GetComponent<MainCharController>();
        // CTO
        FindAndInitNPCsOfType(SpawnableCharactersType.DIAMONDZIK, 1);
        FindAndInitNPCsOfType(SpawnableCharactersType.BIG_G, 1);
        // Unique programmer
        FindAndInitNPCsOfType(SpawnableCharactersType.SYNALENDZIK, 1);
        programmersCount--;
        // Unique Artist 1
        FindAndInitNPCsOfType(SpawnableCharactersType.ROBERDZIK, 1);
        graphicsCount--;
        // Unique Artist 2
        FindAndInitNPCsOfType(SpawnableCharactersType.RYSZARDZIK, 1);
        graphicsCount--;


        // Init remaining TESTERs
        FindAndInitNPCsOfType(SpawnableCharactersType.TESTER, testersCount);
        // Init remaining PROGRAMMERs
        FindAndInitNPCsOfType(SpawnableCharactersType.PROGRAMMER, programmersCount);
        // Init remaining GRAPHICs
        FindAndInitNPCsOfType(SpawnableCharactersType.GRAPHIC, graphicsCount);
        // Init remaining GDs
        FindAndInitNPCsOfType(SpawnableCharactersType.GAME_DESIGNER, gameDesignerCount);


        // Find not occupied Desks and init as 'empty'
        var emptyWorkplaces = workplaces.FindAll(x => !x.occupied);
        foreach (var workplace in emptyWorkplaces)
        {
            workplace.Deactivate();
        }
    }

    void FindAndInitNPCsOfType(SpawnableCharactersType type, int charsToSpawn)
    {
        var workplaces = this.workplaces.FindAll(x => x.characterToSpawn == type && !x.occupied);

        if (workplaces.Count < charsToSpawn)
        {
            Debug.LogError("Not enough workplaces for char of type: " + type);
        }

        var characterPrefab = GameDataManager.Instance.gameData.charactersData.characterPrefabs.Find(x => x.type == type);

        if (characterPrefab == null)
        {
            Debug.LogErrorFormat("No character's prefab of type: {0} in GameData ! ! !", type);
        }

        for (int i = 0; i < charsToSpawn; i++)
        {
            int random = Random.Range(0, workplaces.Count);

            var characterInstance = Instantiate(characterPrefab, transform);
            characters.Add(characterInstance);

            // Put character in a proper workspace
            workplaces[random].Activate(characterInstance);
            workplaces.RemoveAt(random);
        }
    }

    public void DeactivateChars()
    {
        mainChar.SetInputActive(false);

        FindObjectOfType<AI_ControllerD>().Deactivate();
        FindObjectOfType<AI_ControllerG>().Deactivate();
    }

    public void ResetChars()
    {
        //Destroy all characters
        var chars = FindObjectsOfType<BaseCharacterController>();
        foreach (var character in chars)
        {
            Destroy(character.gameObject);
        }

        // DE-occupy workspaces
        foreach (var workplace in workplaces)
        {
            workplace.Deactivate();
        }
    }

}
