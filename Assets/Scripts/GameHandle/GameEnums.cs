﻿public class GameEnums
{

    public enum GameStates
    {
        BEFORE_LOBBY, PLAYING, PAUSED, FINISHED
    }

    public enum CharacterTaskTypes
    {
        SELF, TESTER, GAME_DESIGNER, GRAPHIC, PROGRAMMER, NONE_EMPTY
    }

    public enum SpawnableCharactersType
    {
        NONE_EMPTY, MAIN_CHAR, TESTER, GAME_DESIGNER, GRAPHIC, PROGRAMMER, SYNALENDZIK, ROBERDZIK, RYSZARDZIK, BIG_G, DIAMONDZIK
    }

    public enum TaskStatusType
    {
        CURRENT, DONE, WAITING
    }

    public enum ObstaclesTypes
    {
        DEFAULT, GLASS, CONCRETE
    }

    public enum SoundType
    {
        UI, JINGLE, SFX
    }

}
