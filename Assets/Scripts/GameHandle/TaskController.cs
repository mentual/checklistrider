﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static GameEnums;

public class TaskController : MonoBehaviour
{

    // // // Fields
    List<TaskData> taskList;
    TaskData currentTask;
    WorkplaceController currentWorkspace;
    int currentTaskID;
    TaskDataSO taskData;

    UI_TaskScreen taskView;

    public TaskData CurrentTask { get => currentTask; }


    // // // Methods
    public void Init(UI_TaskScreen taskView)
    {
        this.taskView = taskView;
    }

    public void CreateTasks()
    {
        // Creating tasks means we start from SCRATCH!
        currentTaskID = 0;

        // Take data for everyCreation (might change between.. at least in Editor :D)
        taskData = GameDataManager.Instance.gameData.taskData;


        // Create new List for tasks
        taskList = new List<TaskData>();


        //Duplicating potentialTasks
        List<TaskData> potentialTasks = taskData.taskList.ToList();


        // Pickup minimum number of tasks for all required char types
        PickupTasks(ref potentialTasks, ref taskList, CharacterTaskTypes.SELF);
        PickupTasks(ref potentialTasks, ref taskList, CharacterTaskTypes.PROGRAMMER);
        PickupTasks(ref potentialTasks, ref taskList, CharacterTaskTypes.GRAPHIC);
        PickupTasks(ref potentialTasks, ref taskList, CharacterTaskTypes.GAME_DESIGNER);
        PickupTasks(ref potentialTasks, ref taskList, CharacterTaskTypes.TESTER);


        // Pickup remaining tasks
        int tasksLeft = taskData.tasksToDo - taskList.Count;

        for (int i = 0; i < tasksLeft; i++)
        {
            // Pickup task
            TaskData pickedTask = SelectTask(potentialTasks);

            if (pickedTask != null)
            {
                // Remove from list
                potentialTasks.Remove(pickedTask);

                // Add to current Task list
                taskList.Add(pickedTask);
            }
        }


        // Shuffle picked tasks with extension method
        taskList.Shuffle();

        taskView.Reset();
        taskView.FeedTasks(taskList);
    }

    void PickupTasks(ref List<TaskData> potentialTasks, ref List<TaskData> tasks, CharacterTaskTypes charType)
    {
        var taskPool = potentialTasks.FindAll(x => x.taskType == charType);

        int tasksToFind = 0;
        switch (charType)
        {
            case CharacterTaskTypes.SELF:
                tasksToFind = taskData.selfTasksMin;
                break;
            case CharacterTaskTypes.TESTER:
                tasksToFind = taskData.testerTaskMin;
                break;
            case CharacterTaskTypes.GAME_DESIGNER:
                tasksToFind = taskData.gdTasksMin;
                break;
            case CharacterTaskTypes.GRAPHIC:
                tasksToFind = taskData.artistTasksMin;
                break;
            case CharacterTaskTypes.PROGRAMMER:
                tasksToFind = taskData.programmerTasksMin;
                break;
        }

        for (int i = 0; i < tasksToFind; i++)
        {
            // Pickup task
            TaskData pickedTask = SelectTask(taskPool);

            if (pickedTask != null)
            {
                // Remove from both lists
                taskPool.Remove(pickedTask);
                potentialTasks.Remove(pickedTask);

                // Add to current Task list
                tasks.Add(pickedTask);
            }
        }
    }


    TaskData SelectTask(List<TaskData> taskPool)
    {
        if (taskPool.Count < 1)
        {
            Debug.LogError("Task pool is exhausted! RETURNING NULL");
            return null;
        }

        int randomTaskNo = Random.Range(0, taskPool.Count);
        return taskPool[randomTaskNo];
    }

    public void StartTask()
    {
        currentTask = taskList[currentTaskID];
        taskView.StartTask(currentTaskID);
        GameController.Instance.UiController.Hud.CurrentTaskText.text = taskList[currentTaskID].taskDescription;

        if (currentTask.taskType == CharacterTaskTypes.SELF) // SELF TASK if>0 CAN DUPLICATE -> only one workspace is OURs
        {
            currentWorkspace = GameController.Instance.CharController.Workplaces.Find(x => x.workCharacterTaskType == currentTask.taskType && x.occupied);
        }
        else
        {
            currentWorkspace = GameController.Instance.CharController.Workplaces.Find(x => x.workCharacterTaskType == currentTask.taskType && x.occupied && !x.alreadyDoneTask);
        }

        if (currentWorkspace == null)
            Debug.LogError("No workspace found of type: " + currentTask.taskType);
        currentWorkspace.InitTask();
    }

    public void CompleteCurrentTask()
    {
        // Play Sound
        AudioManager.Instance.PlayTaskCompleted(currentWorkspace.chairTr.position);

        currentWorkspace.CompleteTask();

        string taskCompletionTime = TimerController.SecondsToText(GameController.Instance.TimerController.GetCurrentTime());
        taskView.CompleteTask(currentTaskID, taskCompletionTime);

        GameController.Instance.UiController.CompleteTask();
        // Iterate task
        currentTaskID++;

        // Check tasks condition
        if (currentTaskID >= taskList.Count)
        {
            GameController.Instance.WinGame();
            return;
        }


        // Start new task
        StartTask();
    }

#if PROGRAMMER_DEBUG
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F8))
        {
            CompleteCurrentTask();
        }
    }
#endif

}
