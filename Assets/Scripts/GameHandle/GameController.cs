﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Mentual.Patterns;

public class GameController : BaseStateMachine //With singleton
{

    // // // Fields
    private static GameController instance;
    public static GameController Instance { get => instance; }

    [Header("Refs")]
    [SerializeField] CharController charController;
    [SerializeField] UIController uiController;
    [SerializeField] TaskController taskController;
    [SerializeField] TimerController timerController;
    public CharController CharController { get => charController; }
    public UIController UiController { get => uiController; }
    public TaskController TaskController { get => taskController; }
    public TimerController TimerController { get => timerController; }


    UnityAction OnSceneLoaded;


    // // // Unity
    public virtual void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this) //check if instance already exists (eg. duplicated/reloading scene...)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        SubscribeEvents();

        ChangeState(new GameStartingState());

        taskController.Init(uiController.TaskScreen);

        if (!SceneManager.GetSceneByBuildIndex(1).isLoaded)
        {
            StartCoroutine(LoadSceneAsyncC());
        }
    }


    // // // Methods
    // Self
    void SubscribeEvents()
    {
        OnSceneLoaded += uiController.EnablePlay;

        uiController.OnPressPlay += StartGame;
        uiController.OnPressReset += ResetGame;
        timerController.OnTimerTimout += GameOver;
    }

    IEnumerator LoadSceneAsyncC()
    {
        var loadingSync = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!loadingSync.isDone)
        {
            yield return null;
        }

        OnSceneLoaded?.Invoke();
    }

    #region StatesHandle
    public void StartGame()
    {
        AudioManager.Instance.PlayUI_Play();
        ChangeState(new GamePlayingState());
    }


    public void ResetGame()
    {
        ChangeState(new GameStartingState());
        AudioManager.Instance.PlayUI_Restart();
    }

    public void WinGame()
    {
        int currentRecord = PlayerData.Instance.GetRecord();
        int currentTime = timerController.GetCurrentTime();

        if (currentTime < currentRecord || currentRecord == 0)
        {
            PlayerData.Instance.SaveRecord(currentTime);
        }

        ChangeState(new GameWinState());
    }

    public void GameOver()
    {
        ChangeState(new GameOverState());
    }

    public void CompleteTask()
    {
        taskController.CompleteCurrentTask();
    }
    #endregion


    //Utils Methods
    public void ShowTasks()
    {
        AudioManager.Instance.PlayTaskTabOpen();
        AudioManager.Instance.SetPauseSnapshot(0.03f);

        Time.timeScale = GameDataManager.Instance.gameData.timeScaleTaskTab;

        uiController.SetTaskScreenActive(true);
    }
    public void HideTasks()
    {
        AudioManager.Instance.PlayTaskTabClose();
        AudioManager.Instance.SetGamePlaySnapshot(0.3f);

        Time.timeScale = GameDataManager.Instance.gameData.timeScalePlay;

        uiController.SetTaskScreenActive(false);
    }

    public void HurtPlayer(float amount)
    {
        uiController.HurtPlayer(amount);
    }

    public void TaskProgress(float amount)
    {
        uiController.TaskProgress(amount / taskController.CurrentTask.timeToFinish);
    }

}
