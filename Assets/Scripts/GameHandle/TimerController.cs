﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public struct TimeData
{
    public int startHour;
    public int endHour;
    [Tooltip("How much time level should last. IN SECONDS")]
    public int gameplayRealTime;
    [Tooltip("When the Player should start hearing ticking sounds")]
    public int timeToStartTicking;
}

public class TimerController : MonoBehaviour
{

    // // // Fields
    [SerializeField] AudioSource tickSource;

    TextMeshProUGUI timerText;
    TimeData data;

    bool canUpdateTimer;
    float realTime;
    int currentSecond;

    // Actions
    public UnityAction OnTimerTimout;



    // // // Unity
    private void Update()
    {
        UpdateTimer();
    }


    // // // Methods
    public void InitTimer(TimeData clockData, TextMeshProUGUI timerText)
    {
        data = clockData;

        this.timerText = timerText;
        timerText.text = string.Empty;

        ResetTimer();

        StartTimer(data.startHour);
    }

    public void ResetTimer()
    {
        StopTimer();
        realTime = 0f;
        currentSecond = 0;
        canUpdateTimer = false;
    }

    void StartTimer(int startHour)
    {
        canUpdateTimer = true;

        UpdateTimerText(startHour * 60);
    }

    public void StopTimer()
    {
        tickSource.Stop();
        canUpdateTimer = false;
    }

    void End()
    {
        UpdateTimerText(data.endHour * 60);

        OnTimerTimout?.Invoke();

        StopTimer();
    }


    void UpdateTimer()
    {
        if (!canUpdateTimer)
            return;

        realTime += Time.deltaTime;


        if (realTime > data.gameplayRealTime)
        {
            End();
        }
        else
        {
            // Update only if change -> String GC omit
            int wantedSec = GetCurrentTime();

            if (wantedSec != currentSecond)
            {
                currentSecond = wantedSec;
                UpdateTimerText(GetCurrentTime());
            }

            // Check for Clock ticking sound
            if (realTime > data.timeToStartTicking)
            {
                if (!tickSource.isPlaying)
                    tickSource.Play();
            }
        }

    }

    void UpdateTimerText(int seconds)
    {
        timerText.text = SecondsToText(seconds);
    }


    public int GetCurrentTime()
    {
        return (int)(Mathf.Lerp(data.startHour, data.endHour, realTime / data.gameplayRealTime) * 60);
    }


    public static string SecondsToText(int seconds)
    {
        return (seconds / 60).ToString("N0") + ":" + (seconds % 60).ToString("D2");
    }

}
